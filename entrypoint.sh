#!/bin/bash

export LISTENUP_APP_CONFIG=production

if [[ -f /.passwords ]]; then
  source /.passwords
fi
if [[ -f /.config ]]; then
  source /.config
fi

DEBUG=$1
if [[ -z "$DEBUG" ]]; then
   DEBUG=""
else
    shift
fi

if [[ "$DEBUG" == "--debug" ]]; then
    WSGI="wsgi_debug"
    CELERY_DEBUG="--debug true"
    CMD_ARGS="--bind=0.0.0.0:5000 --workers=1"
else
    WSGI="wsgi"
    CELERY_DEBUG=""
    CMD_ARGS="--bind=0.0.0.0:5000 --workers=6"
fi

# Create or upgrade database
python manage.py db upgrade

if [[ "${SERVICE_URL}" != "" ]]; then
    python -c "from listenup_common.container import register; register()" &
fi

# This line tells celery that it is ok to run as root user... may change later...
export C_FORCE_ROOT=true

python manage.py celery ${CELERY_DEBUG} &

python manage.py parse &

if [[ "$DEBUG" == "--live-reload" ]]; then
    source .env/bin/activate
    export FLASK_APP=${WSGI}
    export FLASK_ENV=development
    export FLASK_DEBUG=1
    flask run --host=0.0.0.0
else
    GUNICORN_CMD_ARGS=${CMD_ARGS} /usr/local/bin/gunicorn ${WSGI}:app
fi
