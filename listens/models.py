from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.utils import guid
from listenup_common.logging import get_logger

logger = get_logger(__name__)

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class ListensForPodcast(BaseModel):
    __tablename_ = 'podcast_listens'
    guid = db.Column(db.String(32), nullable=False, unique=True)
    podcast_guid = db.Column(db.String(32), nullable=False, unique=True)
    count = db.Column(db.BigInteger, default=0, nullable=False)

    def __init__(self, podcast_guid: str):
        self.guid = guid()
        self.podcast_guid = podcast_guid
        self.count = 0

    def to_dict(self):
        item_dict = {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'guid': self.guid,
            'podcast_guid': self.podcast_guid,
            'count': self.count,
        }

        return item_dict

    def increment(self):
        self.count += 1


class LastTimeForUser(BaseModel):
    __tablename__ = 'last_time_for_user'
    guid = db.Column(db.String(32), nullable=False, unique=True)
    podcast_guid = db.Column(db.String(32), nullable=False)
    user_guid = db.Column(db.String(32), nullable=False)
    time_seconds = db.Column(db.Integer, default=0, nullable=False)

    def __init__(self, podcast_guid: str, user_guid: str):
        self.guid = guid()
        self.podcast_guid = podcast_guid
        self.user_guid = user_guid
        self.time_seconds = 0

    def to_dict(self):
        return {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'guid': self.guid,
            'podcast_guid': self.podcast_guid,
            'user_guid': self.user_guid,
            'time_seconds': self.time_seconds,
        }


class ParsingHistory(BaseModel):
    __tablename__ = 'parsing_history'
    guid = db.Column(db.String(32), nullable=False, unique=True)
    start_time = db.Column(db.DateTime, nullable=False)
    end_time = db.Column(db.DateTime, nullable=False)
    processing = db.Column(db.Boolean, nullable=False, default=True)
    failed = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self):
        self.guid = guid()

    def to_dict(self):
        return {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'guid': self.guid,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'processing': self.processing,
            'failed': self.failed,
        }
