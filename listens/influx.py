from typing import List
from influxdb import InfluxDBClient
from influxdb.resultset import ResultSet
from listenup_common.logging import get_logger
from flask import current_app as app
from listens import db
from sqlalchemy import desc
from listens.models import ParsingHistory, ListensForPodcast, LastTimeForUser
import time
import datetime

logger = get_logger(__name__)

LISTEN_MEASUREMENT = "listen"
PODACST_GUID_KEY = "podcast_guid"
USER_GUID_KEY = "user_guid"
TIME_SEC_KEY = "time_seconds"
TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def get_influx_client(host: str, port: str, username: str, password: str, database: str, ssl: bool = True,
                      verify_ssl: bool = False, create: bool = True):
    influx_client = InfluxDBClient(host=host, port=port, username=username, password=password, database=database,
                                   ssl=ssl, verify_ssl=verify_ssl)
    if create:
        try:
            dblist = influx_client.get_list_database()
            db_found = False
            for db in dblist:
                if db['name'] == database:
                    db_found = True
            if not db_found:
                logger.info('Database <%s> not found, trying to create it', database)
                influx_client.create_database(database)
        except Exception as e:
            logger.error('Error creating/querying influxdb database: %s', e)
    return influx_client


def record_listen(influx_listendb: InfluxDBClient, podcast_guid, user_guid, time_seconds):
    logger.debug("Recording listen for podcast: %s user: %s, time: %s", podcast_guid, user_guid, time_seconds)
    point = [{
        "measurement": LISTEN_MEASUREMENT,
        "tags": {
            PODACST_GUID_KEY: podcast_guid,
        },
        "fields": {
            USER_GUID_KEY: user_guid,
            "time_seconds": time_seconds,
        },
    }]
    influx_listendb.write_points(point)


def query_since_time(influx_listendb: InfluxDBClient, time_start, time_end):
    query = "SELECT * FROM " + LISTEN_MEASUREMENT + " WHERE time > \'" + time_start.strftime(TIME_FORMAT) \
            + "\' AND time <= \'" + time_end.strftime(TIME_FORMAT) + "\'"
    logger.info("Running query: " + query)
    return influx_listendb.query(query)


def find_or_create_record():
    logger.info("Finding or creating a record to work on")
    failed_and_not_processing = ParsingHistory.query.filter_by(failed=True).all()
    if failed_and_not_processing and len(failed_and_not_processing) > 0:
        return failed_and_not_processing[0]
    else:
        latest: List[ParsingHistory] = ParsingHistory.query.order_by(desc(ParsingHistory.end_time)).limit(1).all()
        if latest and len(latest) > 0:
            logger.debug("Using existing entry to get new times")
            new_parse = ParsingHistory()
            new_parse.start_time = latest[0].end_time
            new_parse.end_time = datetime.datetime.now()
        else:
            logger.debug("Creating new history from epoch")
            new_parse = ParsingHistory()
            new_parse.start_time = datetime.datetime.utcfromtimestamp(0)
            new_parse.end_time = datetime.datetime.now()
        db.session.add(new_parse)
        db.session.commit()
        return new_parse

# TODO not exactly sure how listens need to be counted... kind of a hard problem when considering awful people
# TODO verify podcast exists


def update_podcast_from_result(result):
    podcast_guid = result[PODACST_GUID_KEY]
    time_seconds = result[TIME_SEC_KEY]
    if time_seconds < 20:
        listens = ListensForPodcast.query.filter_by(podcast_guid=podcast_guid).one_or_none()
        if not listens:
            listens = ListensForPodcast(podcast_guid=podcast_guid)
            logger.debug("Created listen count for: %s", podcast_guid)
        listens.increment()
        db.session.add(listens)
    else:
        logger.debug("Ignoring listen due to not at beginning")


def update_user_from_result(result):
    podcast_guid = result[PODACST_GUID_KEY]
    user_guid = result.get(USER_GUID_KEY, "anonymous")
    time_seconds = result[TIME_SEC_KEY]
    if user_guid and podcast_guid:
        user_time = LastTimeForUser.query.filter_by(user_guid=user_guid, podcast_guid=podcast_guid).one_or_none()
        if not user_time:
            user_time = LastTimeForUser(podcast_guid=podcast_guid, user_guid=user_guid)
            logger.debug("Created user location for user: %s podcast: %s", user_guid, podcast_guid)
        user_time.time_seconds = time_seconds
        db.session.add(user_time)
    else:
        logger.debug("Skipping user location due to user guid: %s or podcast guid: %s was null", user_guid, podcast_guid)

# TODO this will not work if instance is scaled more than 1 due to not race conditions for database updating
# currently waiting for end of everything to update the database but incrementing in the middle


def parse_influx(influx_listendb: InfluxDBClient):
    logger.info("Parsing influx db periodically")
    parse_this = None
    while True:
        try:
            parse_this = find_or_create_record()
            results: ResultSet = query_since_time(influx_listendb=influx_listendb,
                                                  time_start=parse_this.start_time,
                                                  time_end=parse_this.end_time)
            logger.info("Processing: " + str(len(results)) + " entries from influx db")
            logger.info(results)
            for result in results.get_points():
                logger.debug("Processing entry for podcast: " + str(result[PODACST_GUID_KEY]) + " and user: " + str(result.get(USER_GUID_KEY, "anonymous")))
                update_podcast_from_result(result)
                update_user_from_result(result)
            parse_this.processing = False
            parse_this.failed = False
            db.session.add(parse_this)
            db.session.commit()
        except Exception as ex:
            # Try Again??
            if parse_this:
                parse_this.processing = False
                parse_this.failed = True
                db.session.add(parse_this)
                db.session.commit()
            logger.exception("Fatal error when trying to parse influxdb", exc_info=True)
        logger.debug("Sleeping for: " + str(app.config.get("PARSE_DELAY", 10)))
        time.sleep(app.config.get("PARSE_DELAY", 10))
