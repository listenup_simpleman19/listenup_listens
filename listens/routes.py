from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g
from listenup_common.utils import require_json
from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from listenup_common.auth import token_auth, token_optional_auth
from listenup_common.logging import get_logger
from listens.tasks import test_task
from listens.influx import record_listen
from sqlalchemy import desc
from . import db, influx_listens
from listens.models import ListensForPodcast, LastTimeForUser

main = Blueprint('main', __name__, url_prefix='/api/listens')

logger = get_logger(__name__)


@main.route("/test")
def test():
    test_task.apply_async(args=[], countdown=15)
    return ApiResult(value={"hi": "this is a test"}, status=200).to_response()


@main.route("/record", methods=['POST'])
@token_optional_auth.login_required
@require_json(required_attrs=['podcast_guid', 'time_seconds'])
def record(json):
    record_listen(influx_listendb=influx_listens, podcast_guid=json['podcast_guid'],
                  user_guid=g.current_user_guid, time_seconds=json['time_seconds'])
    return ApiMessage(status=200, message="Successfully recorded listen").to_response()


@main.route("/podcast/<string:guid>")
def get_listens(guid: str):
    listens: ListensForPodcast = ListensForPodcast.query.filter_by(podcast_guid=guid).one_or_none()
    if listens:
        return_val = listens.to_dict()
    else:
        logger.warn("Could not find listen count for podcast: %s", guid)
        return_val = ListensForPodcast(podcast_guid=guid).to_dict()
    return ApiResult(value=return_val).to_response()


@main.route("/location/<string:guid>")
@token_optional_auth.login_required
def get_location(guid: str):
    if g.current_user_guid:
        location: LastTimeForUser = LastTimeForUser.query.filter_by(podcast_guid=guid, user_guid=g.current_user_guid).one_or_none()
        if location:
            return_val = location.to_dict()
        else:
            logger.debug("Returning empty last time because one was not found")
            return_val = LastTimeForUser(podcast_guid=guid, user_guid=g.current_user_guid).to_dict()
    else:
        return_val = LastTimeForUser(podcast_guid=guid, user_guid="").to_dict()
    return ApiResult(value=return_val).to_response()


@main.route("/top/<int:number>")
def get_top_x(number: int):
    highest_counts = ListensForPodcast.query.order_by(desc(ListensForPodcast.count)).limit(number).all()
    return ApiResult(value=[highest_count.to_dict() for highest_count in highest_counts]).to_response()


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
