import os
import sys
from listens.models import db
from listens.influx import get_influx_client
from flask import Flask, request, session
from flask_migrate import Migrate
from listenup_common.logging import get_logger
from celery import Celery
from config import config

from listens import models

migrate = Migrate()

celery_broker = 'redis://:' + os.environ.get('REDIS_PASSWORD') + '@' + os.environ.get('REDIS') + '/6'

logger = get_logger(__name__)

# TODO move to common
def start_debug(port):
    sys.path.append("pycharm-debug.egg")
    sys.path.append("pycharm-debug-py3k.egg")

    import pydevd
    pydevd.settrace('192.168.33.1', port=int(port), stdoutToServer=True, stderrToServer=True)


# TODO remove this it will print the password in plain text...
print(celery_broker)

celery = Celery(__name__,
                broker=celery_broker,
                backend=celery_broker)
celery.config_from_object('celeryconfig')

if os.environ.get("DEBUG_PORT", None):
    start_debug(os.environ.get("DEBUG_PORT"))

from listens import tasks

influx_listens = None


def create_app(config_name=None):
    global influx_listens
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    celery.conf.update(config[config_name].CELERY_CONFIG)

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    influx_listens = get_influx_client(host=app.config['INFLUX_DATABASE_IP'],
                                       port=app.config['INFLUX_DATABASE_PORT'],
                                       username=app.config['INFLUX_DATABASE_USER'],
                                       password=app.config['INFLUX_DATABASE_PASS'],
                                       database=app.config['INFLUX_DATABASE_LISTENS'],
                                       ssl=True, verify_ssl=False, create=True)

    # Register web application routes
    from .routes import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
