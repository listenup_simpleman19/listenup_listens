import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY',
                                '51f52814-0071-11e6-a247-000ec6c2372c')
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', SECRET_KEY)
    INFLUX_DATABASE_IP = os.environ.get('INFLUX_DATABASE_IP', None)
    INFLUX_DATABASE_PORT = os.environ.get('INFLUX_DATABASE_PORT', None)
    INFLUX_DATABASE_USER = "listens"
    INFLUX_DATABASE_PASS = os.environ.get('LISTENS_DB_PASSWORD', None)
    INFLUX_DATABASE_LISTENS = "listens"
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', '')
    SQLALCHEMY_POOL_RECYCLE = 60
    CELERY_CONFIG = {}
    URL_PREFIX = '/api/listens'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MARSHALLER_GET_UPLOAD_URL = os.environ.get('MARSHALLER_URL',
                                               'http://listenup_marshaller:5000/api/marshaller/upload/url')


class DevelopmentConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False
    TEMPLATES_AUTO_RELOAD = True
    PARSE_DELAY = 10
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + os.sep + 'listens.db'


class ProductionConfig(Config):
    pass


class TestingConfig(ProductionConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
