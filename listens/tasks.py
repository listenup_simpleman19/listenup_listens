from typing import Optional

from listens import celery
import os

RETRY_TIMES = 5

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')


@celery.task
def test_task():
    print("celery task")


@celery.task
def update_feed(feed_id):
    from listens import create_app
    app = create_app(config_name)
